
def getNextPos(startPos, i):
    direction = i[0]
    value = i[1:]
    endPos = startPos
    if(direction == "U"):
        endPos = [endPos[0]-int(value), endPos[1]]
        axis = 0
    elif(direction == "D"):
        endPos = [endPos[0]+int(value), endPos[1]]
        axis = 0
    elif(direction == "L"):
        endPos = [endPos[0], endPos[1]-int(value)]
        axis = 1
    elif(direction == "R"):
        endPos = [endPos[0], endPos[1]+int(value)]
        axis = 1
    return endPos, axis

def hasCollision(start1, end1, start2, end2):
    if(min(start1[0], end1[0]) < max(start2[0], end2[0]) and max(start1[0], end1[0]) > min(start2[0], end2[0])):
        if(min(start1[1], end1[1]) < max(start2[1], end2[1]) and max(start1[1], end1[1]) > min(start2[1], end2[1])):
            return True
    return False


with open("input.txt") as input:
    wire1 = input.readline()
    wire2 = input.readline()
    endPosWire1 = [0, 0]
    endPosWire2 = [0, 0]
    numberOfSteps1 = 0
    numberOfSteps2 = 0
    minDistance = float('inf')
    for i in str.split(wire1, ","):
        startPosWire1 = endPosWire1
        endPosWire1, axis1 = getNextPos(startPosWire1, i)
        numberOfSteps1 += int(i[1:])
        endPosWire2 = [0, 0]
        numberOfSteps2 = 0
        for j in str.split(wire2, ","):
            startPosWire2 = endPosWire2
            numberOfSteps2 += int(j[1:])
            endPosWire2, axis2 = getNextPos(startPosWire2, j)

            if(axis1 != axis2 and hasCollision(startPosWire1, endPosWire1, startPosWire2, endPosWire2)):
                extraSteps = abs(endPosWire2[1] - endPosWire1[1]) + abs(endPosWire2[0] - endPosWire1[0])
                minDistance = min(numberOfSteps1 + numberOfSteps2 - extraSteps, minDistance)
    print(minDistance)
