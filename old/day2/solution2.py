def getResult(noun, verb, input):
    input[1] = noun
    input[2] = verb
    index = 0
    while(input[index] != 99 and index < len(input) -3):
        if(input[index+1] >= len(input) or input[index+2] >= len(input) or input[index+3] >= len(input)):
            return 0
        if(input[index] == 1):
            input[input[index+3]] = input[input[index+1]] + input[input[index+2]]
        elif(input[index] == 2):
            input[input[index+3]] = input[input[index+1]] * input[input[index+2]]
        index +=4
    return input[0]

with open("input.txt") as file:
    input = file.read().strip().split(',')
    input = [int(i) for i in input]
    noun = 0
    verb = 0
    while(getResult(noun, verb, [i for i in input]) != 19690720):
        noun += 1
        if(noun == 99):
            verb +=1
            noun = 0
        print(noun, verb)
    print(100*noun+verb)
