with open("input.txt") as file:
    input = file.read().strip().split(',')
    input = [int(i) for i in input]
    input[1] = 12
    input[2] = 2
    index = 0
    print(input)
    while(input[index] != 99):
        if(input[index] == 1):
            input[input[index+3]] = input[input[index+1]] + input[input[index+2]]
        elif(input[index] == 2):
            input[input[index+3]] = input[input[index+1]] * input[input[index+2]]
        index +=4
    print(input[0])
