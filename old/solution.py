
number_of_answers = 0
for i in range(128392, 643281):
    has_double = False
    while(i > 10 and i%10 >= i%100//10):
        if(i%100//10 == i%10):
            if(not i%1000//100 == i%10):
                has_double = True
            while(i%1000//100 == i%10):
                i = i//10
        i = i//10
    if(i < 10 and has_double):
        number_of_answers +=1
print(number_of_answers)
