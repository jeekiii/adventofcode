import unittest
from main import get_result, get_result_part_two

class Test(unittest.TestCase):
    def test(self):
        self.assertEqual(get_result("test_input.txt"), 374)
    def test(self):
        self.assertEqual(get_result_part_two("test_input.txt"), 82000210)

if __name__ == '__main__':
    unittest.main()
