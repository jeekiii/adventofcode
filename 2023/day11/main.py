def read_graph(input):
    graph = []
    for line in input.readlines():
        row = []
        graph.append(row)
        for character in line.strip():
            row.append(character)
    return graph

def expand_graph(graph):
    new_graph = []
    for row in graph:
        has_galaxy = False
        for character in row:
            if(character != '.'):
                has_galaxy = True
        new_graph.append(row)
        if(not has_galaxy):
            new_graph.append(row)
    return new_graph

def flip_graph(graph):

    new_graph = []
    for i in range(len(graph[0])):
        new_graph.append([])
    for y, row in enumerate(graph):
        for x, character in enumerate(row):
            new_graph[x].append(character)
    return new_graph

def print_graph(graph):
    for row in graph:
        print(row)


def get_stars(graph):
    stars = []
    for y, row in enumerate(graph):
        for x, character in enumerate(row):
            if(character == '#' and (x, y) not in stars):
                stars.append((x, y))
    return stars

def get_star_distances(stars):
    total = 0
    visited = set()
    for star in stars:
        visited.add(star)
        for star2 in stars:
            if(star2 not in visited):
                distance = abs(star[0] - star2[0]) + abs(star[1] - star2[1])
                total += distance
    return total

def get_expanded_rows(graph):
    expanded_rows = []
    for index, row in enumerate(graph):
        has_galaxy = False
        for character in row:
            if(character != '.'):
                has_galaxy = True
        if(not has_galaxy):
            expanded_rows.append(index)            
    return expanded_rows

def get_star_distances_with_expansion(stars, expanded_rows, expanded_columns, expansion_factor):
    total = 0
    visited = set()
    for star in stars:
        visited.add(star)
        for star2 in stars:
            if(star2 not in visited):
                distance = abs(star[0] - star2[0]) + abs(star[1] - star2[1])
                total += distance
                total += get_expansion_bonus(star, star2, expanded_rows, expanded_columns, expansion_factor)
    return total

def get_expansion_bonus(star, star2, expanded_rows, expanded_columns, expansion_factor):
    min_x, max_x = min(star[0], star2[0]), max(star[0], star2[0])
    min_y, max_y = min(star[1], star2[1]), max(star[1], star2[1])
    
    bonus = 0
    for i in range(min_x, max_x):
        if(i in expanded_columns):
            bonus += expansion_factor -1
    for i in range(min_y, max_y):
        if(i in expanded_rows):
            bonus += expansion_factor -1
    return bonus

def get_result(filename):
    with open(filename) as input:
        graph = read_graph(input)
        graph = expand_graph(graph)
        graph = flip_graph(graph)
        graph = expand_graph(graph)
        graph = flip_graph(graph)
        stars = get_stars(graph)
        return get_star_distances(stars)

def get_result_part_two(filename):
    with open(filename) as input:
        graph = read_graph(input)
        expanded_rows = get_expanded_rows(graph)
        graph = flip_graph(graph)
        expanded_columns = get_expanded_rows(graph)
        graph = flip_graph(graph)
        stars = get_stars(graph)
        expansion_factor = 1000000
        return get_star_distances_with_expansion(stars, expanded_rows, expanded_columns, expansion_factor)


print(get_result("input.txt"))
print(get_result_part_two("input.txt"))