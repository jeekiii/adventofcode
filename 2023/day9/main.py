def get_sequences(input):
    sequences = []
    for line in input:
        sequence = []
        for number in line.strip().split((' ')):
            sequence.append(int(number))
        sequences.append(sequence)
    return sequences

def is_all_zeros(sequence):
    for element in sequence:
        if element != 0:
            return False
    return True

def get_sequence_progression(sequence):
    previous = sequence
    progression = [previous.copy()]
    while(not is_all_zeros(previous)):
        current = []
        for i in range(len(previous) -1):
            current.append(previous[i+1] - previous[i])
        progression.append(current)
        previous = current
    return progression

def predict_next_element(sequence):
    sequence_progression = get_sequence_progression(sequence)[::-1]
    previous = 0
    for sequence in sequence_progression:
        sequence.append(sequence[-1] + previous)
        previous = sequence[-1]
    return sequence_progression[-1][-1]


def get_result(filename):
    with open(filename) as input:
        sequences = get_sequences(input)
        total = 0
        for sequence in sequences:
            total += predict_next_element(sequence)
        return total



def get_result_part_two(filename):
    with open(filename) as input:
        sequences = get_sequences(input)
        total = 0
        for sequence in sequences:
            total += predict_next_element(sequence[::-1])
        return total

print(get_result("input.txt"))
print(get_result_part_two("input.txt"))