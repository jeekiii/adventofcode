max_values = {
    'red': 12,
    'green': 13,
    'blue': 14,
}

def get_result(filename):
    with open(filename, 'r') as input:
        total = 0
        for line in input.readlines():
            game_number = line.split(':')[0].split(' ')[1]
            is_possible = True
            games = line.split(':')[1].split(';')            
            for game in games:
                game_values = game.split(',')
                for game_value in game_values:
                    number = game_value.strip().split(' ')[0]
                    color = game_value.strip().split(' ')[1]
                    if int(number) > max_values[color]:
                        is_possible = False
            if(is_possible):
                total += int(game_number)
        return total


def get_result_part_two(filename):
    with open(filename, 'r') as input:
        total = 0
        for line in input.readlines():
            game_number = line.split(':')[0].split(' ')[1]
            min_values = {
                'red': 0,
                'green': 0,
                'blue': 0,
            }
            games = line.split(':')[1].split(';')
            for game in games:
                game_values = game.split(',')
                for game_value in game_values:
                    number = game_value.strip().split(' ')[0]
                    color = game_value.strip().split(' ')[1]
                    if int(number) > min_values[color]:
                        min_values[color] = int(number)
            total += min_values['red'] * min_values['green'] * min_values['blue']
        return total


print(get_result("input.txt"))
print(get_result_part_two("input.txt"))