import unittest
from main import get_result, get_result_part_two

class Test(unittest.TestCase):
#    def test(self):
#        self.assertEqual(get_result("test_input.txt"), 8)
    def test(self):
        self.assertEqual(get_result_part_two("test_input.txt"), 2286)

if __name__ == '__main__':
    unittest.main()
