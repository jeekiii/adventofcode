import re
import itertools
from itertools import permutations 
 
def get_result(filename):
    with open(filename) as input:
        total = 0
        symbols = {}
        lines = input.readlines()
        for y, line in enumerate(lines):
            for x, character in enumerate(line.strip()):
                if(not character.isdigit() and character != '.'):
                    symbols[(x, y)] = True
        for y, line in enumerate(lines):
            for m in re.finditer("[0-9]+", line):
                x = m.start(0)
                number = m.group(0)
                length = len(number)
                if(find_number_around(length, x, y, symbols)):
                    total += int(number)
        return total

                            
def find_number_around(length, x, y, symbols):
    for i in range(x-1, x + length + 1):
        for j in range(y - 1, y + 1 + 1):
            if (i, j) in symbols:
                return True


            


def get_result_part_two(filename):
    with open(filename) as input:
        total = 0
        gears = {}
        lines = input.readlines()
        for y, line in enumerate(lines):
            for x, character in enumerate(line.strip()):
                if(character == '*'):
                    gears[(x, y)] = []
        for y, line in enumerate(lines):
            for m in re.finditer("[0-9]+", line):
                x = m.start(0)
                number = m.group(0)
                length = len(number)
                gears_around = find_gears_around(length, x, y, gears)
                for gear in gears_around:
                    gears[gear].append(int(number))
        for key in gears:
            if len(gears[key]) == 2:
                total += gears[key][0] * gears[key][1]
        return total

def find_gears_around(length, x, y, gears):
    result = []
    for i in range(x-1, x + length + 1):
        for j in range(y - 1, y + 1 + 1):
            if (i, j) in gears:
                result.append((i , j))
    return result


print(get_result("input.txt"))
print(get_result_part_two("input.txt"))