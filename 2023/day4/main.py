def get_result(filename):
    with open(filename) as input:
        total = 0
        for line in input.readlines():
            winning_numbers = line.split(':')[1].split('|')[0].strip().split(' ')
            winning_dict = {}
            for number in winning_numbers:
                if(number != ''):
                    winning_dict[number.strip()] = True

            numbers_you_have = line.split(':')[1].split('|')[1].split(' ')
            subtotal = 0
            for number in numbers_you_have:
                if number.strip() in winning_dict:
                    if(subtotal == 0):
                        subtotal = 1
                    else:
                        subtotal *= 2
            total += subtotal
        return total
            

def points_won_on_line(line):
    winning_numbers = line.split(':')[1].split('|')[0].strip().split(' ')
    winning_dict = {}
    for number in winning_numbers:
        if(number != ''):
            winning_dict[number.strip()] = True

    numbers_you_have = line.split(':')[1].split('|')[1].split(' ')
    subtotal = 0
    for number in numbers_you_have:
        if number.strip() in winning_dict:
            subtotal += 1
    return subtotal

def get_result_part_two(filename):
    with open(filename) as input:
        total = 0
        scratchcards_per_line = {}
        for line in input.readlines():
            line_number = int(line.split(':')[0].split(' ')[-1])
            if (line_number not in scratchcards_per_line):
                scratchcards_per_line[line_number] = 1
            else:
                scratchcards_per_line[line_number] += 1
            points_won = points_won_on_line(line)
            for i in range(line_number+1, line_number+1+points_won):
                if(i not in scratchcards_per_line):
                    scratchcards_per_line[i] = scratchcards_per_line[line_number]
                else:
                    scratchcards_per_line[i] += scratchcards_per_line[line_number]
        return sum(scratchcards_per_line.values())

print(get_result("input.txt"))
print(get_result_part_two("input.txt"))