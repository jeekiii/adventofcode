from dataclasses import dataclass

@dataclass
class Race:
    time: int 
    distance: int


def parse_list(line):
    result = []
    strings = line.strip().split(':')[1].strip().split(' ')
    for string in strings:
        if(string != ''):
            result.append(int(string))
    return result

def create_races(times, distances):
    races = []
    for time, distance in zip(times, distances):
        races.append(Race(time, distance))
    return races

def get_distance(race, time_pressed):
    return (race.time - time_pressed) * time_pressed

#just to check
def get_optimum(race):
    time_pressed = race.time//2
    print(get_distance(race, time_pressed))

def find_min(race):
    upper_bound = race.time //2
    lower_bound = 1
    time_pressed = (upper_bound + lower_bound) // 2
    distance = get_distance(race, time_pressed)

    while(not (get_distance(race, time_pressed -1) <= race.distance and get_distance(race, time_pressed) > race.distance)):
        time_pressed = (upper_bound + lower_bound) // 2
        distance = get_distance(race, time_pressed)
        if(distance < race.distance):
            lower_bound = time_pressed
        else:
            upper_bound = time_pressed
    
    return time_pressed
            
    


def get_result(filename):
    with open(filename) as input:
        times = parse_list(input.readline())
        distances = parse_list(input.readline())
        races = create_races(times, distances)
        total = 1
        for race in races:
            minimum = find_min(race)
            maximum = race.time - minimum
            possibilities = maximum - minimum +1
            total *= possibilities
        return total
        

def get_result_part_two(filename):
    with open(filename) as input:
        times = parse_list_part_two(input.readline())
        distances = parse_list_part_two(input.readline())
        races = create_races(times, distances)
        total = 1
        for race in races:
            minimum = find_min(race)
            maximum = race.time - minimum
            possibilities = maximum - minimum +1
            total *= possibilities
        return total
        
def parse_list_part_two(line):
    return [int(line.replace(' ', '').split(':')[1].strip())]


print(get_result("input.txt"))
print(get_result_part_two("input.txt"))