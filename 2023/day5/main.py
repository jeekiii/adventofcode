from dataclasses import dataclass


@dataclass
class Range:
    value: int
    length: int

def get_seeds(line):
    strings = line.split(':')[1].strip().split(' ')
    numbers = []
    for string in strings:
        numbers.append(int(string.strip()))
    return numbers

def get_conversion_values(line):
    strings = line.strip().split(' ')
    end = int(strings[0].strip())
    start = int(strings[1].strip())
    length = int(strings[2].strip())
    return start, Range(end, length)

def get_conversion_strings(line):
    start = line.strip().split(' ')[0].split('-')[0]
    end = line.strip().split(' ')[0].split('-')[2]
    return start, end


def get_mapped_value(seed, keys_map, values_maps):
    current_key = 'seed'
    current_value = seed
    while(current_key != 'location'):
        starts = values_maps[current_key].keys()
        for start in starts:
            if(start <= current_value and start + values_maps[current_key][start].length > current_value):
                current_value = values_maps[current_key][start].value + current_value - start
                break
        current_key = keys_map[current_key]
    return current_value
            
def get_result(filename):
    with open(filename) as input:
        seeds = get_seeds(input.readline())
        keys_map = {}
        values_maps = {}
        current_key = ''
        for line in input.readlines():
            if(len(line.strip()) == 0):
                pass
            elif(line.strip()[0].isdigit()):
                start, dest = get_conversion_values(line)
                values_maps[current_key][start] = dest
            else:
                start, end = get_conversion_strings(line)
                current_key = start
                keys_map[start] = end
                values_maps[current_key] = {}
        locations = []

        for seed in seeds:
            locations.append(get_mapped_value(seed, keys_map, values_maps))

        return min(locations)



def get_seed_ranges(line):
    strings = iter(line.split(':')[1].strip().split(' '))
    ranges = []
    for start, length in zip(strings, strings):
        ranges.append(Range(int(start.strip()), int(length.strip())))
    return ranges


def get_range_intersection(start_range, end_range):    
    start = max(start_range.value, end_range.value)
    end = min(start_range.value + start_range.length, end_range.value + end_range.length)
    
    length = end - start
    return Range(start, max(0, length))


def map_ranges(ranges, keys_map, values_maps, current_key):
    if(current_key == 'location'):
        return ranges
    mapped = []
    for input_range in ranges:
        intersections = []
        for start in values_maps[current_key]:
            intersection = get_range_intersection(input_range, Range(start, values_maps[current_key][start].length))
            if(intersection.length > 0):
                mapped_range = map_intersection(intersection, start, values_maps, current_key)
                mapped.append(mapped_range)
                intersections.append(intersection)
        mapped.extend(get_unmapped_parts(intersections, input_range))
    result = check_for_overlap(mapped)
    return map_ranges(result, keys_map, values_maps, keys_map[current_key])

def check_for_overlap(ranges):
    sorted_ranges = sorted(ranges, key = lambda to_sort: to_sort.value)
    unique_ranges = []
    i = 0
    while(i < len(sorted_ranges)):
        if(i+1 < len(sorted_ranges)):
            min_next = min(sorted_ranges[i].value + sorted_ranges[i].length, sorted_ranges[i+1].value)
            if(min_next > sorted_ranges[i].value):
                unique_ranges.append(Range(sorted_ranges[i].value, min_next - sorted_ranges[i].value))
        else:
            unique_ranges.append(sorted_ranges[i])
        i+=1
    return unique_ranges

def get_unmapped_parts(intersections, input_range):

    if len(intersections) == 0:
        return [input_range]
    result = []
    current = input_range.value
    for intersection in sorted(intersections, key = lambda intersection: intersection.value):
        if(intersection.value > current):
            result.append(Range(current, intersection.value - current))
        current = intersection.value + intersection.length

    if(current < input_range.value + input_range.length):
        result.append(Range(current, input_range.value + input_range.length - current))

    return result


def map_intersection(intersection, start, values_maps, current_key):
    offset = intersection.value - start
    mapped_value = values_maps[current_key][start].value + offset
    mapped_length = intersection.length
    return Range(mapped_value, mapped_length)
    

def get_result_part_two(filename):
    with open(filename) as input:
        seed_ranges = get_seed_ranges(input.readline())
        keys_map = {}
        values_maps = {}
        current_key = ''
        for line in input.readlines():
            if(len(line.strip()) == 0):
                pass
            elif(line.strip()[0].isdigit()):
                start, dest = get_conversion_values(line)
                values_maps[current_key][start] = dest
            else:
                start, end = get_conversion_strings(line)
                current_key = start
                keys_map[start] = end
                values_maps[current_key] = {}

        end_ranges = map_ranges(seed_ranges, keys_map, values_maps, 'seed')
        start_values = []
        for end_range in end_ranges:
            start_values.append(end_range.value)
        return min(start_values)


print(get_result("input.txt"))
print(get_result_part_two("input.txt"))