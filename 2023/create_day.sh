#!/bin/bash
#!/bin/bash

# Check if directory day1 exists
if [ -d "day1" ]; then
    # Find the last day directory, sort numerically and get the day part
    lastday=$(ls -d day* | sort -V | tail -n 1 | cut -c 4-)
    currentday=$((lastday + 1))
else
    currentday=1
fi

# Create a new directory for the current day
mkdir "day$currentday"

# Copy contents from base to the new directory
cp base/* "day$currentday/"
