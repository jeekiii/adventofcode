def get_line_sum(line):
    stripped = ""
    for character in line:
        if(character.isdigit()):
            stripped += character
    return int(stripped[0])*10 + int(stripped[-1])

def get_result(filename):
    with open(filename) as input:
        total = 0
        for line in input.readlines():
            total += get_line_sum(line)
        return total

number_dict = {
    "one": "1",
    "two": "2",
    "three": "3",
    "four": "4",
    "five": "5",
    "six": "6",
    "seven": "7",
    "eight": "8",
    "nine": "9",
}

def get_line_sum_with_translation(line):
    stripped = ""
    for index, character in enumerate(line):
        if(character.isdigit()):
            stripped += character
        else:
            for key in number_dict:
                if(line[index:].startswith(key)):
                    stripped += number_dict[key]
    return int(stripped[0])*10 + int(stripped[-1])

def get_result_part_two(filename):
    with open(filename) as input:
        total = 0
        for line in input.readlines():
            total += get_line_sum_with_translation(line)
        return total


print(get_result("input.txt"))
print(get_result_part_two("input.txt"))