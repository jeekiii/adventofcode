from dataclasses import dataclass


card_values = {
    'A': 14,
    'K': 13,
    'Q': 12,
    'J': 11,
    'T': 10,
    '9': 9,
    '8': 8,
    '7': 7,
    '6': 6,
    '5': 5,
    '4': 4,
    '3': 3,
    '2': 2,
}

card_values_with_joker = {
    'A': 14,
    'K': 13,
    'Q': 12,
    'T': 10,
    '9': 9,
    '8': 8,
    '7': 7,
    '6': 6,
    '5': 5,
    '4': 4,
    '3': 3,
    '2': 2,
    'J': 1,
}

hand_values = {
    'five_kind': 7,
    'four_kind': 6,
    'full_house': 5,
    'three_kind': 4,
    'two_pairs': 3,
    'one_pair': 2,
    'high_card': 1,
}

@dataclass
class Hand:
    cards: []
    bid: int
    card_order_value: int
    hand_value: int

def parse_hand(line):
    cards_string = line.strip().split(' ')[0]
    bid = int(line.strip().split(' ')[1])
    cards = []
    for letter in cards_string:
        cards.append(card_values[letter])
    return Hand(cards = cards, bid = int(bid), card_order_value = get_first_card_value(cards), hand_value = get_hand_value(cards))

def get_first_card_value(cards):
    value = 0
    card_weight = 5
    for card in cards:
        value += card * (15 ** card_weight)
        card_weight -=1
    return value
    
def get_hand_value(cards):
    groups = {}
    for card in cards:
        if(card not in groups):
            groups[card] = 1
        else:
            groups[card] += 1


    if(len(groups) == 1):
        return hand_values['five_kind']
    elif(len(groups) == 2):
        if(max(groups.values()) == 4):
            return hand_values['four_kind']
        else:
            return hand_values['full_house']
    elif(len(groups) == 3):
        if(max(groups.values()) == 3):
            return hand_values['three_kind']
        else:
            return hand_values['two_pairs']
    elif(len(groups) == 4):
        return hand_values['one_pair']
    elif(len(groups) == 5):
        return hand_values['high_card']

#_______________

def parse_hand_with_jokers(line):
    cards_string = line.strip().split(' ')[0]
    bid = int(line.strip().split(' ')[1])
    cards = []
    for letter in cards_string:
        cards.append(card_values_with_joker[letter])
    return Hand(cards = cards, bid = int(bid), card_order_value = get_first_card_value(cards), hand_value = get_hand_value_with_jokers(cards))


def get_hand_value_with_jokers(cards):
    groups = {}
    joker_amount = 0
    for card in cards:
        if(card == 1):
            joker_amount += 1
        elif(card not in groups):
            groups[card] = 1
        else:
            groups[card] +=1
    
    #get max group and add it the joker
    if(joker_amount == 5):
        return hand_values['five_kind']
    else:
        max_group = 0
        max_group_value = 0
        for card_type in groups:
            if(groups[card_type] > max_group_value):
                max_group = card_type
                max_group_value = groups[card_type]
        groups[max_group] += joker_amount


    if(len(groups) == 1):
        return hand_values['five_kind']
    elif(len(groups) == 2):
        if(max(groups.values()) == 4):
            return hand_values['four_kind']
        else:
            return hand_values['full_house']
    elif(len(groups) == 3):
        if(max(groups.values()) == 3):
            return hand_values['three_kind']
        else:
            return hand_values['two_pairs']
    elif(len(groups) == 4):
        return hand_values['one_pair']
    elif(len(groups) == 5):
        return hand_values['high_card']

def get_key(hand):
    return hand.hand_value * (15 ** 6) + hand.card_order_value


def get_result(filename):
    with open(filename) as input:
        hands = []
        for line in input:
            hand = parse_hand(line)
            hands.append(hand)
        total = 0
        for index, hand in enumerate(sorted(hands, key=get_key)):
            total += hand.bid * (index +1)
            
        return total

def get_result_part_two(filename):
    with open(filename) as input:
        hands = []
        for line in input:
            hand = parse_hand_with_jokers(line)
            hands.append(hand)
        total = 0
        for index, hand in enumerate(sorted(hands, key=get_key)):
            total += hand.bid * (index +1)
            
        return total


print(get_result("input.txt"))
print(get_result_part_two("input.txt"))