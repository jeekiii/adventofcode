import unittest
from main import get_result, get_result_part_two

class Test(unittest.TestCase):
    # def test(self):
    #     self.assertEqual(get_result("test_input.txt"), 2)
    # def test(self):
    #     self.assertEqual(get_result("test_input2.txt"), 6)
    def test(self):
        self.assertEqual(get_result_part_two("test_input3.txt"), 6)

if __name__ == '__main__':
    unittest.main()
