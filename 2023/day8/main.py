from dataclasses import dataclass
import math

def get_directions(line):
    return list(line.strip())

def parse_nodes(input):
    nodes = {}
    for line in input:
        origin = line.strip().split( )[0]
        node = {}
        node['L'] = line.strip()[7:10]
        node['R'] = line.strip()[12:15]
        nodes[origin] = node
    return nodes
        

def get_result(filename):
    with open(filename) as input:
        directions = get_directions(input.readline())
        input.readline()
        nodes = parse_nodes(input)
        current = 'AAA'
        steps = 0
        while(current != 'ZZZ'):
            direction = directions[steps % len(directions)]
            current = nodes[current][direction]
            steps +=1
        return steps
        

def get_result_part_two(filename):
    with open(filename) as input:
        directions = get_directions(input.readline())
        input.readline()
        nodes = parse_nodes(input)

        current_nodes = []

        for node in nodes:
            if node[-1] == 'A':
                current_nodes.append(node)

        steps = 0
        initial_starts = current_nodes.copy()
        cycle_start_nodes = current_nodes.copy()
        distances_to_z = {}
        next_cycle_start = {}

        all_cycles_computed = False
        while(not all_cycles_computed):
            direction = directions[steps % len(directions)]
            is_finished = True


            
            if(steps % len(directions) == 0 and steps != 0):
                all_cycles_computed = True
                for index, node in enumerate(current_nodes):
                    if(cycle_start_nodes[index] not in next_cycle_start):
                        all_cycles_computed = False
                    next_cycle_start[cycle_start_nodes[index]] = node
                cycle_start_nodes = current_nodes.copy()
            
            for index, current in enumerate(current_nodes):
                if(current[-1] != 'Z'):
                    is_finished = False
                else:
                    if(cycle_start_nodes[index] not in distances_to_z):
                        distances_to_z[cycle_start_nodes[index]] = []
                    distances_to_z[cycle_start_nodes[index]].append(steps % len(directions))
                
                current_nodes[index] = nodes[current][direction]

            if(is_finished):
                return steps
            steps +=1
        return get_match(directions, initial_starts, distances_to_z, next_cycle_start)
    
def get_match(directions, initial_starts, distances_to_z, next_cycle_start):
    has_match = False
    
    cycles = 0
    current_nodes = initial_starts.copy()
    final_dist_to_z = []
    super_cycles = {}
    offsets = {}

    for node in initial_starts:
        z_node, offsets[node] = find_offset(node, next_cycle_start)
        super_cycles[node] = find_cycle(z_node, next_cycle_start)
    print(offsets)
    print(super_cycles)
    return math.lcm(*list(super_cycles.values())) * len(directions)
    


def find_cycle(node, next_cycle_start):
    cycle_length = 1
    current = next_cycle_start[node]
    while(current != node):
        current = next_cycle_start[current]
        cycle_length += 1

    return cycle_length

def find_offset(node, next_cycle_start):
    offset = 1
    current = next_cycle_start[node]
    while(not current[-1] == 'Z'):
        current = next_cycle_start[current]
        offset += 1
    return current, offset
        
    


#print(get_result("input.txt"))
print(get_result_part_two("input.txt"))